# Ansible Role:

Install and configure firewalld in RHEL/Fedora, Debian/Ubuntu.

## Requirements

None

## Role Variables

None

## Dependencies

Include in requirements of playbook fivem

  - name: ansible.posix
    version: 1.3.0
    source: https://galaxy.ansible.com

## Example Playbook

    - hosts: servers
      roles:
         - { role: dsg-role-firewall }

## License

MIT

## Author Information

This role was created in 2021 by Didier MINOTTE.
